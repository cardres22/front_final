import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {photo}from '../interfaces/photo';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  URI = 'http://localhost:4000/api/photos';
  constructor(private http: HttpClient) {

    
   }
   createPhoto(titulo:string, desc: string, foto:File){
      const formulario = new FormData();
      formulario.append('title',titulo);
      formulario.append('description',desc);
      formulario.append('image', foto);
      return this.http.post(this.URI, formulario);
  }
  getphoto(){
    return this.http.get<photo[]>(this.URI);
  }
  pre_photo(id:string){
    return this.http.get<photo>(this.URI + '/' + id);
  }
  deletephoto(id:string){
    return this.http.delete(this.URI + '/' + id);
  }
  updatephoto(id:string, title:string, desc:string){
    return this.http.put(this.URI+'/'+ id,{title,desc});
  }
}
