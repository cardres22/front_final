import { Component, OnInit } from '@angular/core';
import {PhotoService} from '../../services/photo.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.css']
})
export class PhotoListComponent implements OnInit {

  photos = [];
  constructor(private photoservice:PhotoService, private router:Router) { }

  ngOnInit(): void {
    this.photoservice.getphoto().subscribe(res => {
      this.photos=res;
    }, err => console.log(err));
  }
  selected_card(id:string){
    this.router.navigate(['photos/',id]);
  }

}
