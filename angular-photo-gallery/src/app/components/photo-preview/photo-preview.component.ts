import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {PhotoService} from '../../services/photo.service';
import {photo} from '../../interfaces/photo';
@Component({
  selector: 'app-photo-preview',
  templateUrl: './photo-preview.component.html',
  styleUrls: ['./photo-preview.component.css']
})
export class PhotoPreviewComponent implements OnInit {
  id:string;
  photo:photo;
  
  constructor(
    private active:ActivatedRoute,
    private router:Router,
    private photoService:PhotoService
  ) { }

  ngOnInit(): void {
    this.active.params.subscribe(params=> {
      this.id=params['id'];
      this.photoService.pre_photo(this.id).subscribe(
        res=>this.photo=res,
        err=>console.log(err)
      )
    })
  }
  deletephoto(id:string){
    this.photoService.deletephoto(id).subscribe(
      res=>this.router.navigate(['/photos']),
      err=>console.log(err)
    );
  }
  updatephoto(id:string, title:string, desc:string){
    this.photoService.updatephoto(id,title,desc).subscribe(
      res=>this.router.navigate(['/photos']),
      err=>console.log(err)
    );
  }

}
